package services;

import dao.SomeRepository;

import java.util.ArrayList;
import java.util.List;

public class SomeService implements SomeRepository {
    private List<Integer> list;

    public SomeService() {
        list = new ArrayList<>();
    }

    public void save(Integer num) {
        list.add(num);
    }

    public void update(Integer num){
        list.add(num);
    }
}
